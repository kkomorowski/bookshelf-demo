name := "bookshelf-demo"

version := "0.0.1"

scalaVersion := "2.12.7"

libraryDependencies ++=
  Seq(
    "com.typesafe.akka" %% "akka-http" % "10.1.5",
    "com.typesafe.akka" %% "akka-actor" % "2.5.18",
    "de.heikoseeberger" %% "akka-http-circe" % "1.22.0",
    "io.circe" %% "circe-generic" % "0.10.1",
    "io.circe" %% "circe-generic-extras" % "0.10.1",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
    "ch.qos.logback" % "logback-classic" % "1.2.3"
  )

enablePlugins(JavaServerAppPackaging)
enablePlugins(DockerPlugin)
enablePlugins(AshScriptPlugin)

mainClass in Compile := Some("pl.hiquality.bookshelf.demo.WebServer")

dockerBaseImage := "openjdk:jre-alpine"
dockerUsername := Some("hiqualitypl")
