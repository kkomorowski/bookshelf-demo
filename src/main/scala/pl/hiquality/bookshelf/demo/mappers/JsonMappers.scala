package pl.hiquality.bookshelf.demo.mappers

import akka.http.scaladsl.model.DateTime
import io.circe.{Decoder, DecodingFailure, Encoder, Json}
import pl.hiquality.bookshelf.demo.model._

object JsonMappers {
  import io.circe.generic.extras.semiauto._
  import io.circe.generic.extras.Configuration

  implicit val customConfig: Configuration =
    Configuration.default
      .withDefaults
      .withSnakeCaseMemberNames

  implicit val endpointsEncoder: Encoder[Endpoints] = deriveEncoder

  implicit val endpointsDecoder: Decoder[Endpoints] = deriveDecoder

  implicit val errorEncoder: Encoder[Error] = deriveEncoder

  implicit val errorDecoder: Decoder[Error] = deriveDecoder

  implicit val userEncoder: Encoder[User] = deriveEncoder

  implicit val userDecoder: Decoder[User] = deriveDecoder

  implicit val userLinksEncoder: Encoder[UserLinks] = deriveEncoder

  implicit val userLinksDecoder: Decoder[UserLinks] = deriveDecoder

  implicit val isbnEncoder: Encoder[Isbn] = deriveUnwrappedEncoder

  implicit val isbnDecoder: Decoder[Isbn] = deriveUnwrappedDecoder

  implicit val titleEncoder: Encoder[Title] = deriveUnwrappedEncoder

  implicit val titleDecoder: Decoder[Title] = deriveUnwrappedDecoder

  implicit val authorEncoder: Encoder[Author] = deriveUnwrappedEncoder

  implicit val authorDecoder: Decoder[Author] = deriveUnwrappedDecoder

  implicit val dateTimeDecoder: Decoder[DateTime] = Decoder.instance { c =>
    c.as[String] match {
      case Right(s) => DateTime.fromIsoDateTimeString(s)
                               .map(Right(_))
                               .getOrElse(Left(DecodingFailure("datetime", c.history)))
      case l @ Left(_) => l.asInstanceOf[Decoder.Result[DateTime]]
    }
  }

  implicit val dateTimeEncoder: Encoder[DateTime] = Encoder.instance(date =>
    Json.fromString(date.toIsoDateTimeString())
  )

  implicit val bookEncoder: Encoder[Book] = deriveEncoder

  implicit val bookDecoder: Decoder[Book] = deriveDecoder
}
