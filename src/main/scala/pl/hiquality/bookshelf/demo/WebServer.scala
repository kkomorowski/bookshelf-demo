package pl.hiquality.bookshelf.demo

import java.security.MessageDigest

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.model.{DateTime, StatusCodes}
import akka.http.scaladsl.model.headers.EntityTag
import akka.http.scaladsl.server.AuthenticationFailedRejection.{CredentialsMissing, CredentialsRejected}
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.Credentials
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import pl.hiquality.bookshelf.demo.mappers.JsonMappers._
import pl.hiquality.bookshelf.demo.model._

import scala.concurrent.{ExecutionContextExecutor, Future}

object WebServer extends HttpApp with App with LazyLogging {

  class UnauthorizedException extends Exception
  class AlreadyExistsException extends Exception

  @volatile var library: List[Book] = List.empty

  def findBookByIsbn(username: String, isbn: Isbn): Future[Option[Book]] = Future {
    library
      .find(item => item.owner.contains(username) && item.isbn == isbn)
  }

  def deleteBookByIsbn(username: String, isbn: Isbn): Future[Done] = {
    library = library.filterNot(item => item.owner.contains(username) && item.isbn == isbn)
    Future(Done)
  }

  def listBooks(username: String): Future[List[Book]] = Future {
    library.filter(_.owner.contains(username))
  }

  def listPublicBooks: Future[List[Book]] = Future {
    library.filter(_.public)
  }

  def addBook(book: Book): Future[Done] = {
    library = book :: library
    Future(Done)
  }

  def updateBook(book: Book): Future[Option[Book]] = {
    for {
      _ <- findBookByIsbn(book.owner.getOrElse(""), book.isbn)
      _ <- deleteBookByIsbn(book.owner.getOrElse(""), book.isbn)
      _ <- addBook(book)
    } yield Option(book)
  }

  @volatile var users = List.empty[User]
  @volatile var tokens = Map.empty[String, String]

  def addUser(user: User) =
    users.find(_.username == user.username) match {
      case None => users ::= user
        Future(UserLinks(user))
      case Some(_) => Future.failed(new AlreadyExistsException)
    }

  def deleteUser(username: String, authUser: String) = {
    if (username == authUser) {
      users = users.filter(_.username != username)
      tokens = tokens.filter(_._2 != username)
      Future(StatusCodes.NoContent)
    } else Future.failed(new UnauthorizedException)
  }

  def addToken(username: String) = {
    val token = java.util.UUID.randomUUID().toString
    tokens = tokens.filter(_._2 != username) + (token -> username)
    Future { token }
  }

  def userAuthenticator(credentials: Credentials): Option[String] =
    credentials match {
      case p @ Credentials.Provided(id)
        if p.verify(
          users.find(_.username == id)
            .map(_.password).getOrElse("")
        ) => Some(id)
      case _ => None
    }

  def oauthAuthenticator(credentials: Credentials): Option[String] =
    credentials match {
      case Credentials.Provided(token) => tokens.get(token)
      case _ => None
    }

  implicit val system: ActorSystem = ActorSystem("bookshelf-actor-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  implicit def myRejectionHandler: RejectionHandler =
    RejectionHandler.newBuilder()
      .handle { case AuthenticationFailedRejection(CredentialsMissing, _) =>
        complete((StatusCodes.Unauthorized, Error("You didn't provide your credentials!")))
      }
      .handle { case AuthenticationFailedRejection(CredentialsRejected, _) =>
        complete((StatusCodes.Unauthorized, Error("Invalid credentials!")))
      }
      .handle { case AuthorizationFailedRejection =>
        complete((StatusCodes.Forbidden, Error("You're out of your depth!")))
      }
      .handle{ case MalformedRequestContentRejection(msg, _) =>
        complete((StatusCodes.BadRequest, Error("The request content was malformed: " + msg)))
      }
      .handle { case ValidationRejection(msg, _) =>
        complete((StatusCodes.BadRequest, Error("That wasn't valid! " + msg)))
      }
      .handleAll[MethodRejection] { methodRejections =>
        val names = methodRejections.map(_.supported.name)
        complete((StatusCodes.MethodNotAllowed, Error(s"Can't do that! Supported: ${names mkString " or "}!")))
      }
      .handleNotFound { complete((StatusCodes.NotFound, Error("Not here!"))) }
      .result()

  val userRoute =
    path("users") {
      post {
        entity(as[User]) { user =>
          completeOrRecoverWith(addUser(user)) { _ =>
            complete((StatusCodes.Conflict, Error("User of such name already exists!")))
          }
        }
      }
    } ~
    pathPrefix("users" / Segment) { username =>
      path("token") {
        get {
          authenticateBasic("Bookshelf Secured", userAuthenticator) { authUser =>
            logger.debug(s"Token for $username")
            logger.debug(s"Authenticated user $authUser")
            logger.debug(s"$users")
            onSuccess(addToken(username)) { token =>
              logger.debug(s"$tokens")
              complete(token)
            }
          }
        }
      } ~
      pathEnd {
        delete {
          authenticateOAuth2("Bookshelf Secured", oauthAuthenticator) { authUser =>
            completeOrRecoverWith(deleteUser(username, authUser)) { _ =>
              complete(StatusCodes.Forbidden, Error("You are not allowed to do this."))
            }
          }
        }
      }
    }

  val auth = authenticateOAuth2("Bookshelf Secured", oauthAuthenticator)

  val md = MessageDigest.getInstance("MD5")

  def bookETag(book: Book) = {
    book.lastUpdated.map(date => EntityTag(md.digest(date.toIsoDateTimeString().getBytes()).mkString))
  }

  val entrypointRoute =
    get {
      pathSingleSlash {
        complete(Endpoints("/users", "/book"))
      }
    }

  val bookSecureRoute =
    get {
      path("book") {
        auth { authUser: String => complete(listBooks(authUser)) } ~
        complete(listPublicBooks)
      } ~
      pathPrefix("book" / Remaining) { i =>
        auth { authUser =>
          optionalHeaderValueByName("If-None-Match") {
            case Some(header) =>
              onSuccess(findBookByIsbn(authUser, Isbn(i))) {
                case Some(book) =>
                  conditional(bookETag(book), book.lastUpdated) {
                    complete(
                      if (bookETag(book).map(_.tag).contains(header)) StatusCodes.NotModified
                      else (StatusCodes.OK, book)
                    )
                  }
                case None => complete(StatusCodes.NotFound)
              }

            case None =>
              onSuccess(findBookByIsbn(authUser, Isbn(i))) {
                case Some(book) =>
                  conditional(bookETag(book), book.lastUpdated) {
                    complete(book)
                  }
                case None => complete(StatusCodes.NotFound)
              }
          }
        }
      }
    } ~
    post {
      path("book") {
        auth { authUser =>
          entity(as[Book]) { book =>
            onComplete(addBook(book.copy(owner = Some(authUser), lastUpdated = Some(DateTime.now)))) { _ =>
              complete(StatusCodes.Created, book)
            }
          }
        }
      }
    } ~
    put {
      pathPrefix("book" / Remaining) { i =>
        auth { authUser =>
          entity(as[Book]) { book =>
            onSuccess(updateBook(book.copy(isbn = Isbn(i), owner = Some(authUser), lastUpdated = Some(DateTime.now)))) {
              case Some(updatedBook) => complete(updatedBook)
              case None => complete(StatusCodes.NotFound)
            }
          }
        }
      }
    } ~
    delete {
      path("book" / Remaining) { i =>
        auth { authUser =>
          onComplete(deleteBookByIsbn(authUser, Isbn(i))) { _ =>
            complete(StatusCodes.NoContent)
          }
        }
      }
    }

  def routes = handleRejections(myRejectionHandler) { entrypointRoute ~ userRoute ~ bookSecureRoute }

  startServer("0.0.0.0", 8080)

}
