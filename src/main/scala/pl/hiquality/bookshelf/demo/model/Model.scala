package pl.hiquality.bookshelf.demo.model

case class Endpoints(users: String, book: String)

case class Error(message: String)

case class User(username: String, password: String, email: String, age: Int)
case class UserLinks(self: String, token: String)

object UserLinks {
  def apply(user: User): UserLinks =
    new UserLinks(
      self = s"/users/${user.username}",
      token = s"/users/${user.username}/token"
    )
}

import akka.http.scaladsl.model.DateTime

import scala.collection.immutable.ListSet

case class Isbn(value: String) extends AnyVal
case class Title(value: String) extends AnyVal
case class Author(value: String) extends AnyVal

case class Book(isbn: Isbn, title: Title, authors: ListSet[Author], pageCount: Option[Int], public: Boolean = false, owner: Option[String], lastUpdated: Option[DateTime])